#if UNITY_EDITOR
using System;
using xAPI4Unity.Editor;
using UnityEditor;
using OmiLAXR.Analytics.Editor;
using OmiLAXR.Analytics.xAPI_Registry;

namespace OmiLAXR.Analytics.xAPI4Unity.Editor
{
    [InitializeOnLoad]
    public class xAPIStatements
    {
        private static xAPI_Context_Ide Ide => xAPI_Definitions.ide;
        
        static xAPIStatements()
        {
            // todo repair
            var fetcherWindow = FetcherWindow.Instance;
            if (!fetcherWindow)
                return;
            fetcherWindow.OnTriggeredControl += OnTriggeredControl;
        }

        private static void OnTriggeredControl(Type type, string label, string value)
        {
            AnalyticsHandler.SendStatement(
                verb: Ide.verbs.clicked, // todo -> triggered / changed
                activity: Ide.activities.control,
                activityExtensions: Ide.extensions.activity
                    .type(type.ToString())
                    .name(label)
                    .content(value)
                );
        }
    }
}
#endif